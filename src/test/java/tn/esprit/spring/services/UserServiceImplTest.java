package tn.esprit.spring.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import tn.esprit.spring.entities.Role;
import tn.esprit.spring.entities.User;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class UserServiceImplTest {

	@Autowired
	IUserService us;
	
	@Test
	@Order(1)
	public void testretrieveAllUsers() {
		List<User> listUsers = us.retrieveAllUsers();
		Assertions.assertEquals(0,listUsers.size());
	}
	
	
	
	@Test
	@Order(2)
	public void testaddUser() throws ParseException {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = dateFormat.parse("2015-03-23");
		
		User u = new User(1L, "youssef", "jemai", d, Role.TECHNICIEN);
		User userAdded = us .addUser(u);
		
		Assertions.assertEquals(u.getLastName(),userAdded.getLastName());

	    
	}
	
	
	@Test
	@Order(3)
	public void testupdateUser() throws ParseException {
		 
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date d = dateFormat.parse("2015-03-23");
		
		User u = new User(1L,"youssef22", "jemai22", d, Role.TECHNICIEN);
		User userUpdated = us.updateUser(u);
		Assertions.assertEquals(u.getLastName(),userUpdated.getLastName());
	}
	
	
	@Test
	@Order(4)
	public void retrieveUser() {
		
		User  userRetrieved = us.retrieveUser("1");
		Assertions.assertEquals(1L,userRetrieved.getId());
	}
	
	
	@Test 
	@Order(5)
	public void testdeleteUser() {
		us.deleteUser("1");
		Assertions.assertNull(us.retrieveUser("1"));

	}
	
	
	
	}
	
